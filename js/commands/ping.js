module.exports = {
  name: "ping",
  description: "Botの反応速度をチェックします。",
  execute(message) {
    message.channel
      .send("確認中です…")
      .then(pingcheck =>
        pingcheck.edit(`反応速度 : ${pingcheck.createdTimestamp -
              message.createdTimestamp}ms`)
      );
    return;
  }
};