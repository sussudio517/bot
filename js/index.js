const http = require('http');
http.createServer(function (req, res) {
  res.write("online");
  res.end();
}).listen(8080);
/**
 * Module Imports
 */
const { Client, Collection } = require("discord.js");
const { readdirSync } = require("fs");
const { join } = require("path");
const { ReactionController } = require("discord.js-reaction-controller");
const serp = require("serp");

let TOKEN, PREFIX;
  TOKEN = process.env.TOKEN;
  PREFIX = process.env.PREFIX;

const client = new Client({ disableMentions: "everyone" });

client.login(TOKEN);
client.commands = new Collection();
client.prefix = PREFIX;
client.queue = new Map();
const cooldowns = new Collection();
const escapeRegex = (str) => str.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");

/**
 * Client Events
 */
client.on("ready", () => {
  console.log(`${client.user.username} ready!`);
  client.user.setPresence({
    activity: {
      name: `${PREFIX}help`,
    },
    status: "online"
  });
  client.channels.cache.get('732844033663303730').send({
    embed: {
      title: "システムが再起動しました。",
      description: "次回のbump通知はありません。",
      color: 7506394
    }
  });
});
client.on("warn", (info) => console.log(info));
client.on("error", console.error);

/**
 * Import all commands
 */
const commandFiles = readdirSync(join(__dirname, "commands")).filter((file) => file.endsWith(".js"));
for (const file of commandFiles) {
  const command = require(join(__dirname, "commands", `${file}`));
  client.commands.set(command.name, command);
}

client.on("message", async (message) => {
  if (message.author.bot) return;
  if (!message.guild) return;

  const prefixRegex = new RegExp(`^(<@!?${client.user.id}>|${escapeRegex(PREFIX)})\\s*`);
  if (!prefixRegex.test(message.content)) return;

  const [, matchedPrefix] = message.content.match(prefixRegex);

  const args = message.content.slice(matchedPrefix.length).trim().split(/ +/);
  const commandName = args.shift().toLowerCase();

  const command =
    client.commands.get(commandName) ||
    client.commands.find((cmd) => cmd.aliases && cmd.aliases.includes(commandName));

  if (!command) return;

  if (!cooldowns.has(command.name)) {
    cooldowns.set(command.name, new Collection());
  }

  const now = Date.now();
  const timestamps = cooldowns.get(command.name);
  const cooldownAmount = (command.cooldown || 1) * 1000;

  if (timestamps.has(message.author.id)) {
    const expirationTime = timestamps.get(message.author.id) + cooldownAmount;

    if (now < expirationTime) {
      const timeLeft = (expirationTime - now) / 1000;
      const cooldowntimenow = await message.channel.send(`\`${command.name}\`コマンドを使用するには、あと${timeLeft.toFixed(1)}秒待ってください。`)
      cooldowntimenow.delete({ timeout: 5000 })
      return message.delete();
    }
  }

  timestamps.set(message.author.id, now);
  setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);

  try {
    command.execute(message, args);
  } catch (error) {
    console.error(error);
    message.reply("エラーが発生しました。コマンドが正しいか確認してください。").catch(console.error);
  }

  // こっから下要整理

  // botメッセージは拒否
  if (message.author.bot) return;

  // DM
  if (message.channel.type == "dm") {
    if (message.author.id == client.user.id) {
      return;
    }
    message.channel.send(
      "サーバーへの招待リンクです。\nhttps://discord.gg/r2ng7Eu"
    );
  }

  // 掲示板

  // create
  if (message.channel.id == '712220900878975076') {
    const crch = await message.guild.channels.create(message.content, { parent: '712220796449456238', topic: '<@!' + message.author.id + '> の質問' });
    const crs = await message.channel.send(
      {embed: {
        title: "✅ 作成しました",
        color: "#00B06B",
        description: 'チャンネルを作成しました。\n<#'+crch.id+'>'
      }}
    );
        crs.delete({ timeout: 60000 });
    const situmonuser = message.mentions.users.first() || message.author;
    const situmona = message.mentions.users.first() || message.author;
    const keijicr = new discord.MessageEmbed()
	.setColor('#00B06B')
	.setAuthor(situmonuser.username, `https://cdn.discordapp.com/avatars/${message.author.id}/${message.author.avatar}.png`)
	.setDescription(message.content)
	.setTimestamp()
	.setFooter('解決したらcloseと送信');
    crch.send(keijicr);
  }

  // close
  else if (message.content.startsWith('close')) {
    if (message.channel.parentID == '712220796449456238') {
      try {
        var spl1 = message.channel.topic.split('<@!')[1]
        var spl2 = spl1.split('>')[0]
      }
      catch (error) {
        var spl2 = '0'
      }
      console.log(spl2 + '\n' + message.author.id)
      if (!(spl2 == message.author.id || message.author.id == '612599805310402561' || message.author.id == '418670054574391316' || message.author.id == '723052392911863858' || message.author.id == '682838384909090841')) {
        const closepermn = await message.channel.send(
          {embed: {
            title: "⚠ 操作を完了出来ません",
            color: "#F2E700",
            description: 'チャンネルを解決済みに出来るのは質問者と管理者のみです。'
          }}
        )
        closepermn.delete({ timeout: 5000 })
        return;
      }
      message.channel.edit({ parentID: '712222487798349845' })
      message.channel.send(
        {embed: {
           title: "✅ 解決済み",
           color: "#00B06B",
           description: '<@!' + message.author.id + '>がチャンネルを解決済みにしました。\n<#769742311365804092>にクリア編成を掲載していただけるとありがたいです！'
        }}
      )
    } else {
      const closedekinai = await message.channel.send(
        {embed: {
           title: "⚠ 操作を完了出来ません",
           color: "#F2E700",
           description: 'このチャンネルは掲示板ではないため解決済みには出来ません。'
        }}
      )
      closedekinai.delete({ timeout: 5000 })
    }
  }

  // open
  else if (message.content.startsWith('open')) {
    if (message.channel.parentID == '712222487798349845') {
      try {
        var spl1 = message.channel.topic.split('<@!')[1]
        var spl2 = spl1.split('>')[0]
      }
      catch (error) {
        var spl2 = '0'
      }
      console.log(spl2 + '\n' + message.author.id)
      if (!(spl2 == message.author.id || message.author.id == '612599805310402561' || message.author.id == '418670054574391316' || message.author.id == '723052392911863858' || message.author.id == '682838384909090841')) {
        const openkengen = await message.channel.send(
          {embed: {
             title: "⚠ 操作を完了出来ません",
              color: "#F2E700",
             description: 'チャンネルを未解決にできるのは質問者と管理者のみです。'
          }}
        )
        openkengen.delete({ timeout: 5000 })
        return;
      }
      message.channel.edit({ parentID: '712220796449456238' })
      message.channel.send(
        {embed: {
           title: "💡 未解決",
           color: "#00B06B",
           description: '<@!' + message.author.id + '>がチャンネルを未解決にしました。'
        }}
      )
    }
    else {
      const opendekinai = await message.channel.send(
        {embed: {
           title: "⚠ 操作を完了出来ません",
           color: "#F2E700",
           description: 'このチャンネルは掲示板ではないため未解決には出来ません。'
        }}
      )
      opendekinai.delete({ timeout: 5000 })
    }
  }

  // delete
  else
    if (message.content.startsWith('delete')) {
      if (message.channel.parentID == '712222487798349845') {
        try {
          var spl1 = message.channel.topic.split('<@!')[1]
          var spl2 = spl1.split('>')[0]
        }
        catch (error) {
          var spl2 = '0'
        }
        console.log(spl2 + '\n' + message.author.id)
        if (!(/*spl2 == message.author.id||*/message.author.id == '612599805310402561' || message.author.id == '418670054574391316' || message.author.id == '723052392911863858' || message.author.id == '682838384909090841')) {
          const deletedekinai = await message.channel.send(
            {embed: {
              title: "⚠ 操作を完了出来ません",
              color: "#F2E700",
              description: 'チャンネルを削除できるのは管理者のみです。'
            }}
          )
          deletedekinai.delete({ timeout: 5000 })
          return;
        }
        var deledcname = message.channel.name
        message.channel.send(
        {embed: {
           title: ":x: 削除",
           color: "#00B06B",
           description: '削除の準備中です。しばらくお待ちください。'
        }})
        setTimeout(function() {
          try {
            message.channel.delete('解決した質問')
          } catch (err) {
            console.error(err)
          }
        }, 5000)

      } else if (message.channel.parentID == '712220796449456238') {
        const closesitene = await message.channel.send(
          {embed: {
           title: "⚠ 操作を完了出来ません",
           color: "#F2E700",
           description: 'このチャンネルは解決済みにされていないため削除出来ません。'
          }}
        )
        closesitene.delete({ timeout: 5000 })
      } else {
        const deletedekinai = await message.channel.send(
          {embed: {
             title: "⚠ 操作を完了出来ません",
             color: "#F2E700",
             description: 'このチャンネルは掲示板ではないため削除出来ません。'
          }}
        )
        deletedekinai.delete({ timeout: 5000 })
      }
    }
  
  // db検索
  if (message.content.startsWith(".db")) {
    try{
    const db_search = message.content.slice("4");
    const db_webs = message.content.slice("4");
    const db_console = message.content.slice("4");
    var options = {
      host: "google.co.jp",
      qs: {
        q: db_search + "+site:https://battlecats-db.com/",
        filter: 0,
        pws: 0
      },
      num: 3
    };
    const links = await serp.search(options);
    message.channel.send({
      embed: {
        title: "検索結果",
        description: `[${links[0].title}](https://www.google.co.jp${links[0].url})\n\n[${links[1].title}](https://www.google.co.jp${links[1].url})\n\n[${links[2].title}](https://www.google.co.jp${links[2].url})\n\nすべての検索結果は[こちら](https://bcserver.gitlab.io/dbsearch/?q=`+db_webs+")",
        color: "#B06000"
      }
    });
    console.log(`検索ワード:${db_console}\n結果:\n${links[0].title}(${links[0].url})\n${links[1].title}(${links[1].url})\n${links[2].title}(${links[2].url}`)
  } catch (error) {
    const db_notfound = message.content.slice("4");
    message.channel.send({
      embed: {
        description: "検索結果がありません。\n[こちら](https://bcserver.gitlab.io/dbsearch/?q="+db_notfound+")で探しているものが見つかるかもしれません。",
        color: "#B06000"
      }
    });
  }
  }

  // google検索
  if (message.content.startsWith(".g")) {
    try{
    const g_search = message.content.slice("3");
    const g_webs = message.content.slice("3");
    var options = {
      host: "google.co.jp",
      qs: {
        q: g_search,
        filter: 0,
        pws: 0
      },
      num: 3
    };
    const links = await serp.search(options);
    message.channel.send({
      embed: {
        title: "検索結果",
        description: `[${links[0].title}](https://www.google.co.jp${links[0].url})\n\n[${links[1].title}](https://www.google.co.jp${links[1].url})\n\n[${links[2].title}](https://www.google.co.jp${links[2].url})`,
        color: "#B06000"
      }
    });
  } catch (error) {
    const g_notfound = message.content.slice("4");
    message.channel.send({
      embed: {
        description: "検索結果がありません。",
        color: "#B06000"
      }
    });
  }
  }

  // 不適切発言フィルター
  if (message.content.match(/おっぱい|ち●こ|ちんこ|ちんちん|死ね|殺す|マンコ|オナニー/)) {
    if(message.channel.id === "648038994839470114") return;
      message.delete();
      console.log(message.content+" by "+message.author.username);
      const reply = await message.reply('不適切な発言は控えましょう');
      reply.delete({ timeout: 5000 })
  }

  // eval
  if (command === prefix + "eval") {
    if (message.author.id !== "723052392911863858") return;

    let evaled;
    try {
      evaled = await eval(args.join(" "));
      message.channel.send(inspect(evaled));
    } catch (error) {
      message.channel.send(
        {embed: {
          title: "⚠ 操作を完了出来ません",
          color: "#F2E700",
          description: 'エラー内容：\n```'+error+'```'
        }}
      );
    }
  }

  // ヘルプ
  if (message.content.startsWith(prefix.prefix + 'help')) {
    const controller = new ReactionController(client);
    controller.addPages([
      new discord.MessageEmbed().setTitle('ヘルプ(1/3)').addFields(
        {
          name: '目次',
          value:
            '1, このページ\n2, 標準機能ヘルプ\n3, 音楽機能ヘルプ'
        },
        {
          name: 'ヘルプの使い方',
          value:
            'リアクションで操作できます。◀️で前のページ、▶️で次のページ、⏹で現在のページに固定します。'
        }
      ),
      new discord.MessageEmbed().setTitle('ヘルプ(2/3)').addFields(
        {
          name: prefix.prefix + 'trans [日本語]',
          value: '[日本語]部分を英語に翻訳します。'
        },
        {
          name: prefix.prefix + 'trans [日本語以外]',
          value: '[日本語以外]部分を日本語に翻訳します。'
        },
        {
          name: prefix.prefix + 'trans [元言語]-[翻訳先言語]-[元言語の文]',
          value: '[元言語の文]を[元言語]から[翻訳先言語]に翻訳します。'
        },
        {
          name: prefix.prefix + 'tips',
          value: 'にゃんこ大戦争に関する豆知識を表示します。'
        },
        {
          name: prefix.prefix + 'tips',
          value: 'ランダムにひろかずを表示します。'
        }
      ),
      new discord.MessageEmbed().setTitle('ヘルプ(3/3)').addFields(
        {
          name: prefix.prefix + 'loop (l)',
          value: '曲をループ再生します。',
          inline: true
        },
        {
          name: prefix.prefix + 'lyrics (ly)',
          value: '再生中の曲の歌詞を取得します。',
          inline: true
        },
        {
          name: prefix.prefix + 'np',
          value: '再生中の曲を表示します。',
          inline: true
        },
        {
          name: prefix.prefix + 'play (p)',
          value: 'YouTubeから音楽を再生します。',
          inline: true
        },
        {
          name: prefix.prefix + 'playlist (pl)',
          value: 'YouTubeからプレイリストを再生します。',
          inline: true
        },
        {
          name: prefix.prefix + 'pruning',
          value: 'Botメッセージのプルニングを切り替えます。',
          inline: true
        },
        {
          name: prefix.prefix + 'queue (q)',
          value: 'キューを表示します。',
          inline: true
        },
        {
          name: prefix.prefix + 'remove',
          value: 'キューをクリアします。',
          inline: true
        },
        {
          name: prefix.prefix + 'resume (r)',
          value: '一時停止中の音楽を再生します。',
          inline: true
        },
        {
          name: prefix.prefix + 'search',
          value: '音楽を検索します。',
          inline: true
        },
        {
          name: prefix.prefix + 'shuffle',
          value: 'キューをシャッフルします。',
          inline: true
        },
        {
          name: prefix.prefix + 'skip (s)',
          value: '次の曲へスキップします。',
          inline: true
        },
        {
          name: prefix.prefix + 'skipto (st)',
          value: '指定したキューの曲へスキップします。',
          inline: true
        },
        {
          name: prefix.prefix + 'stop',
          value: '音楽を停止します。',
          inline: true
        },
        {
          name: prefix.prefix + 'volume (v)',
          value: '再生中の音楽の音量を変更します。',
          inline: true
        }
      )
    ]);
    controller.send(message).catch(console.error);
  }

});

// ここから下はbotメッセージを受け入れる
client.on("message", async message => {

  // bump通知
  if (message.author.bot) {
    if (message.author.id == "302050872383242240") {
      if (
        message.embeds[0].color == "2406327" &&
        message.embeds[0].url == "https://disboard.org/" &&
        (message.embeds[0].description.match(/表示順をアップしたよ/) ||
          message.embeds[0].description.match(/Bump done/) ||
          message.embeds[0].description.match(/Bump effectué/) ||
          message.embeds[0].description.match(/Bump fatto/) ||
          message.embeds[0].description.match(/Podbito serwer/) ||
          message.embeds[0].description.match(/Успешно поднято/) ||
          message.embeds[0].description.match(/갱신했어/) ||
          message.embeds[0].description.match(/Patlatma tamamlandı/))
      ) {
        const noti = await message.channel.send({
          embed: {
            title: "Bumpが実行されました！",
            description:
              "再度実行可能になったらお知らせします。",
            color: 7506394
          }
        });
        noti.delete({ timeout: 7200000 });
        setTimeout(() => {
          message.channel.send({
            embed: {
              title: "Bumpできます！",
              description: "コマンド`!d bump`を送信できます。",
              color: 7506394
            }
          });
        }, 7200000);
      } else if (
        message.embeds[0].color == "15420513" &&
        message.embeds[0].url == "https://disboard.org/" &&
        (message.embeds[0].description.match(
          /このサーバーを上げられるようになるまで/
        ) ||
          message.embeds[0].description.match(
            /あなたがサーバーを上げられるようになるまで/
          ))
      ) {
        var splcontent_a = message.embeds[0].description.split("と");
        console.log(splcontent_a[1]);
        var splcontent_b = splcontent_a[1].split("分");
        console.log(splcontent_b[0]);
        var waittime_bump = splcontent_b[0];

        message.channel.send({
          embed: {
            title: "Bumpに失敗したようです…",
            description: waittime_bump + "分後にもう一度お試しください。",
            color: 7506394
          }
        });
      }
    }
  }
});